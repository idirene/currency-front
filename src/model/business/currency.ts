export class Currency {

curncyid  :string;
crncydsc  : string;
crncydsca  :string;
crncysym : string;
currnidx :string;
curtexta_1  :string;
curtexta_2  :string;
curtexta_3  :string;
curtext_1   :string;
curtext_2  :string;
curtext_3  :string;
decsymbl :number;
dex_row_id :number;
dex_row_ts :Date;
inclspac :boolean ;
negsymbl :number;
ngsmampc :number;
thoussym :number;
}
