import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LayoutModule} from '@angular/cdk/layout';
import {
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule, MatNativeDateModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material';
import {MainNavComponent} from './ui-components/main-nav/main-nav.component';
import {AppRoutingModule} from './app-routing.module';
import {RouterModule} from "@angular/router";
import {HttpClientModule, HTTP_INTERCEPTORS} from "@angular/common/http";

import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import { LoginComponent } from './login/login.component';
import {AuthenticationService} from "./_services/authentication.service";
import { RegisterComponent } from './register/register.component';
import {SigneUpRequest} from "../model/admin/signeUpRequest";
import {Role} from "../model/admin/role";
import {User} from "../model/admin/user";
import { HomeComponent } from './home/home.component';
import { Currency } from '../model/business/currency';
import { HomeService } from './_services/home.service';
import { LoginRequest } from 'src/model/admin/LoginRequest';
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { ErrorInterceptor } from './_helpers/error_interceptor';
import { CurrencyComponent } from './currency/currency.component';
import { AddCurrencyComponent } from './currency/add-currency/add-currency.component';
import { EditCurrencyComponent } from './currency/edit-currency/edit-currency.component';
import { AdminstrationComponent } from './adminstration/adminstration.component';

@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    CurrencyComponent,
    AddCurrencyComponent,
    EditCurrencyComponent,
    AdminstrationComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatSlideToggleModule,
    MatExpansionModule,
    MatCardModule,
    MatTableModule,
    MatSortModule,
    MatGridListModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatDialogModule,
    MatInputModule,
    MatSnackBarModule,
    MatSelectModule,
    MatMenuModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [AuthenticationService, LoginRequest, SigneUpRequest, Role,User,Currency,HomeService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
  ]
})
export class AppModule { }
