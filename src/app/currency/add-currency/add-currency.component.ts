import {Component, OnInit,Output,EventEmitter} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { Currency } from '../../../model/business/currency';
import { HomeService } from '../../_services/home.service';
import {NegaSymbole} from "../../../model/business/negSymbol";
import {PasswordValidateur, PasswordValidator} from "../../shared/negsymb.validators";



@Component({
  selector: 'app-add-currency',
  templateUrl: './add-currency.component.html',
  styleUrls: ['./add-currency.component.css']
})
export class AddCurrencyComponent implements OnInit {

  addForm: FormGroup;
  currency: Currency = new Currency();
  negsymbls: NegaSymbole[]=[{code:"()",value: 1},{code:"-",value: 2},{code:"CR",value: 3}];
  constructor(private fb: FormBuilder,
              private homeService: HomeService) { }

             @Output() public childEvent = new EventEmitter();

  ngOnInit() {

    this.addForm = this.fb.group({
      curncyid:['',[        Validators.required, Validators.maxLength(15)      ]],
      crncydsc:['',[        Validators.required,        Validators.maxLength(30)      ]],
      crncydsca:['',[        Validators.required,        Validators.maxLength(30)      ]],
      crncysym:['',[      ]],
      currnidx:['',[      ]],

      curtexta_1:['',[       Validators.required,       Validators.maxLength(150)      ]],
      curtexta_2:['',[        Validators.required      ]],
      curtexta_3:['',[    Validators.maxLength(30)      ]],
      curtext_1:['',[      ]],
      curtext_2:['',[      ]],
      curtext_3:['',[      ]],
      decsymbl:['',[      ]],
      dex_row_id:['',[      ]],
      dex_row_ts:['',[     ]],
      inclspac:['',[      ]],
      negsymbl:['',[        Validators.maxLength(30)      ]],
      ngsmampc:['',[        Validators.maxLength(30)      ]],
      thoussym:['',[        Validators.maxLength(30)      ]],
    },{validator :PasswordValidator});
       this.addForm.get('curncyid').valueChanges.subscribe(value =>{
      console.log('test'+value)});
  }
  ///currency
  // convenience getter for easy access to form fields
  get f() { return this.addForm.controls; }

  onValidationClick(){
    this.currency.curncyid = this.f.curncyid.value;
    this.currency.crncydsc = this.f.crncydsc.value;
    this.currency.crncydsca= this.f.crncydsca.value;
    this.currency.curtext_1= this.f.curtext_1.value;
    this.currency.curtext_2= this.f.curtext_2.value;
    this.currency.curtext_3= this.f.curtext_3.value;
    this.currency.curtexta_1= this.f.curtexta_1.value;
    this.currency.negsymbl= this.f.negsymbl.value;
    this.currency.curtexta_2= this.f.curtexta_2.value;
    this.currency.curtexta_3= this.f.curtexta_3.value;
    this.currency.crncysym= this.f.crncysym.value;
    // this.currency.type = this.f.type.value;
    // this.currency.active = true;


    this.homeService.createCurrency(this.currency).subscribe( (data: Currency) =>{
        console.log("DATA:");
        console.log(data);
        this.currency = data;
      this.firEvent();
      this.currency= new Currency();
        // this.router.navigate([departement.id,{relativeTo:this.route}]);
        // this.router.navigate(['/showpersonne', this.personne.type, this.personne.id ]);/*
        //  this.router.navigate(['/showpersonne', {id: this.personne.id, type: this.personne.type}]);*/
        /*    this.router.navigate(['/typesanction']);*/
      }
    );
  }
  firEvent(){
    this.childEvent.emit('test');
  }

}
