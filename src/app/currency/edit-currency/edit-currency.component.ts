import {Component, OnInit,Output,EventEmitter} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { Currency } from '../../../model/business/currency';
import { HomeService } from '../../_services/home.service';
import {NegaSymbole} from "../../../model/business/negSymbol";
import {ActivatedRoute} from "@angular/router";
import {
  verfierDefaultl, verfierDefaultlZero, AbstractValidateur,
  PasswordValidator
} from "../../shared/negsymb.validators";

@Component({
  selector: 'app-edit-currency',
  templateUrl: './edit-currency.component.html',
  styleUrls: ['./edit-currency.component.css']
})
export class EditCurrencyComponent implements OnInit {

  editForm: FormGroup;
  public negsymb ;
  public currencyId ;
  currency: Currency = new Currency();
  negsymbls: NegaSymbole[]=[{code:"()",value: 1},{code:"-",value: 2},{code:"CR",value: 3}];
  constructor(private fb: FormBuilder,
              private homeService: HomeService,
              private route :ActivatedRoute) {


  }
  getCurrency(){
    this.homeService.getCurrencyById(this.currencyId).subscribe(
      (currency: Currency) =>this.editCurrency(currency),
      (err :any )=> console.log(err));
  }



  ngOnInit() {
    this.editForm = this.fb.group({
      curncyid:['',[        Validators.required, Validators.maxLength(15)]],
      crncydsc:['',[        Validators.required,        Validators.maxLength(30) ,AbstractValidateur(/admin/)      ]],
      crncydsca:['',[        Validators.required,        Validators.maxLength(30)      ]],
      crncysym:['',[      ]],
      currnidx:['',[      ]],

      curtexta_1:['',[       Validators.required,       Validators.maxLength(150)      ]],
      curtexta_2:['',[        Validators.required      ]],
      curtexta_3:['',[    Validators.maxLength(30)      ]],
      curtext_1:['',[      ]],
      curtext_2:['',[      ]],
      curtext_3:['',[      ]],
      decsymbl:['',[      ]],
      dex_row_id:['',[      ]],
      dex_row_ts:['',[     ]],
      inclspac:['',[      ]],
      negsymbl:['',[        Validators.maxLength(30)  ,AbstractValidateur(/0/)   ]],
      ngsmampc:['',[        Validators.maxLength(30)      ]],
      thoussym:['',[        Validators.maxLength(30)      ]],
    },{validator :PasswordValidator});

    let id = this.route.snapshot.paramMap.get('id');
    this.currencyId= id;
    console.log('test id ' +this.currencyId);
    this.getCurrency();
    // this.editForm.get('curncyid')
    this.f.curncyid.disable();
   this.f.negsymbl.valueChanges.subscribe(value =>{

       // const curncy = this.f.curncyid;
       //   curncy.setValidators(Validators.dis)

   });
  }


  // function edit
  editCurrency(currency : Currency ){
    this.currency= currency;
    console.log(this.currency.negsymbl);
    this.negsymb =this.currency.negsymbl;

    this.editForm.patchValue({
      curncyid:this.currency.curncyid,
      crncydsc:this.currency.crncydsc,
      crncydsca:this.currency.crncydsca,
      crncysym:this.currency.crncysym,
      currnidx:this.currency.currnidx,
      curtexta_1:this.currency.curtexta_1,
      curtexta_2:this.currency.curtexta_2,
      curtexta_3:this.currency.curtexta_3,
      curtext_1:this.currency.curtext_1,
      curtext_2:this.currency.curtext_2,
      curtext_3:this.currency.curtext_3,
      decsymbl:this.currency.decsymbl,
      dex_row_id:this.currency.dex_row_id,
      dex_row_ts:this.currency.dex_row_ts,
      inclspac:this.currency.inclspac,
      negsymbl:this.negsymb,
      ngsmampc:this.currency.ngsmampc,
      thoussym:this.currency.thoussym
    });
  }
  ///currency
  // convenience getter for easy access to form fields
  get f() { return this.editForm.controls; }



  onValidationClick(){
    this.currency.curncyid = this.f.curncyid.value;
    this.currency.crncydsc = this.f.crncydsc.value;
    this.currency.crncydsca= this.f.crncydsca.value;
    this.currency.curtext_1= this.f.curtext_1.value;
    this.currency.curtext_2= this.f.curtext_2.value;
    this.currency.curtext_3= this.f.curtext_3.value;
    this.currency.curtexta_1= this.f.curtexta_1.value;
    this.currency.negsymbl= this.f.negsymbl.value;
    this.currency.curtexta_2= this.f.curtexta_2.value;
    this.currency.curtexta_3= this.f.curtexta_3.value;
    this.currency.crncysym= this.f.crncysym.value;
        this.homeService.updateCurrency(this.currency).subscribe( (data: Currency) =>{
        console.log("DATA:");
        console.log(data);
        this.currency = data;
        this.currency= new Currency();
        // this.router.navigate([departement.id,{relativeTo:this.route}]);
        // this.router.navigate(['/showpersonne', this.personne.type, this.personne.id ]);/*
        //  this.router.navigate(['/showpersonne', {id: this.personne.id, type: this.personne.type}]);*/
        /*    this.router.navigate(['/typesanction']);*/
      }
    );
  }

}
