import {AbstractControl,ValidatorFn} from "@angular/forms";

export function verfierDefaultl(control: AbstractControl):{[key:string] : any }|null{
  const  verifier=/default/.test(control.value);
  return verifier ?{'verifierDefault':{value :control.value}}:null;
}
export function verfierDefaultlZero(control: AbstractControl):{[key:string] : any }|null{
  console.log(control.value);
  const  verifier=/0/.test(control.value);
  return verifier ?{'verifierZero':{value :control.value}}:null;
}

export function AbstractValidateur(forbbiden :RegExp):ValidatorFn {

  return (control: AbstractControl):{[key:string] : any }|null =>{
    const  verifier=forbbiden.test(control.value);
    return verifier ?{'verifier':{value :control.value}}:null;
  };
}
export function PasswordValidateur(control: AbstractControl):{[key:string] : boolean }|null {
 const crncydsca = control.get('crncydsca');
 const crncydsc = control.get('crncydsc');
  console.log(' 11111 '+crncydsca.value +' 2222 '+crncydsc.value);
  console.log(' ggggg /// / / / / / / '+crncydsca.value !== crncydsc.value);

    return crncydsc && crncydsca &&(crncydsca.value !== crncydsc.value) ?
    {'misMatch':true}
    :null;

}

export function PasswordValidator(control: AbstractControl): { [key: string]: boolean } | null {
  const crncydsc = control.get('crncydsc');
  const crncydsca = control.get('crncydsca');
  if (crncydsc.pristine || crncydsca.pristine) {
    return null;
  }
  return crncydsc && crncydsca && crncydsc.value !== crncydsca.value ? { 'misMatch': true } : null;
}
