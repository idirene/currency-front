import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login.component'
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './_guards/auth.guard';
import { CurrencyComponent } from './currency/currency.component';
import { RegisterComponent } from './register/register.component';
import {AddCurrencyComponent} from "./currency/add-currency/add-currency.component";
import {EditCurrencyComponent} from "./currency/edit-currency/edit-currency.component";

const routes: Routes = [

  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'currency',
    component: CurrencyComponent  ,
    canActivate: [AuthGuard]
  },
  {
  path: 'addCurr',
    component: AddCurrencyComponent  ,
    canActivate: [AuthGuard]
  },
  {
    path: 'editCurr/:id',
    component: EditCurrencyComponent  ,
    canActivate: [AuthGuard]
  },

  // otherwise redirect to home
  { path: '**', redirectTo: '/' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {


}
