
import { Injectable } from '@angular/core';
import {UtilStatic} from "../_services/UtilStatic";
import {HttpClient} from '@angular/common/http';
import {Currency} from '../../model/business/currency';
import {map} from "rxjs/internal/operators";
import {Observable} from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http: HttpClient) { }

  getCurrency() {
    return this.http.get<Currency[]>( `${UtilStatic.API_SOURCE}currency/`,{observe: 'response'});
  }
    getCurrencyById  (currencyId:string) :Observable<Currency> {
    return this.http.get<any>( `${UtilStatic.API_SOURCE}currency/${currencyId}`);
  }
  getCurrencyList() {
    return this.http.get<any>( `${UtilStatic.API_SOURCE}currency/`,{observe: 'response'}) .pipe(
      map(data => {
        return data.body.content.map(item =>{
          return item;
        });
      }) // or any other operator
    );
  }
  deleteCurrency(id: string){
    return this.http.delete(    `${UtilStatic.API_SOURCE}currency/${id}`);
  }

  createCurrency(currency: Currency) {
    const url = `${UtilStatic.API_SOURCE}currency/add`;
    return this.http.post( url, currency);
  }
  updateCurrency(currency: Currency){
    const url = `${UtilStatic.API_SOURCE}currency/edit`;
    console.log(url);
    return this.http.put( url, currency);
  }
}
